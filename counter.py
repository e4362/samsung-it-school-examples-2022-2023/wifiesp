import random
import time
import paho.mqtt.client as mqtt_client

broker = "broker.emqx.io"


def on_message(client, userdata, message):
    data = str(message.payload.decode("utf-8"))
    topic = str(message.topic.encode("utf-8"))
    print(f"Received message on {topic}: {data}")


client = mqtt_client.Client(f'isu_fbki_{random.randint(10000, 99999)}')
client.on_message = on_message

try:
    client.connect(broker)
except Exception:
    print('Failed to connect, check network')
    exit()

client.loop_start()

print('Subscribing')

client.subscribe("esp8266696D/sensor")
client.subscribe("esp826689EC/sensor")

time.sleep(600)
client.disconnect()
client.loop_stop()
print('Stop communication')

