#include "WIFI.h"
#include "Server.h"
#include "MQTT.h"

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  WiFi_init(false);
  server_init();
  MQTT_init();
  mqtt_cli.publish((mqtt_client_id + "/state").c_str(), "hello");
  mqtt_cli.subscribe((mqtt_client_id + "/state").c_str());
}

void loop() {
  // put your main code here, to run repeatedly :
  server.handleClient();
  mqtt_cli.loop();

  mqtt_cli.publish((mqtt_client_id + "/sensor").c_str(), String(sensor_get_dist()).c_str());
  delay(500);
}
